const express = require('express')
const bodyParser = require('body-parser')

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.get("/",(req,res) => {
    //console.log(req.headers.cmuoauth2user)
    //reponse content with request headers
    res.status(200)
    .send(JSON.stringify(req.headers))
    
})

app.listen(8080,'0.0.0.0')
console.log("Service is listening on port 8080")