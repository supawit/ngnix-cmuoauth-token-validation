# nginx configuration example for validate CMU Oauth access token.

from article https://www.nginx.com/blog/validating-oauth-2-0-access-tokens-nginx

rename **nginx.env.dist** to **nginx.env** and use your CMU Oauth client_id, client_secret, redirect_uri, scope

then

```
docker-compose up
```

# Testing
1. Browse to http://localhost:8080/login
2. Get access token after login
3. access application with access token
```
curl -H "Authorization: Bearer 0BgkG8uCxGNdraPZHxDz9WcJ39zKtTng" http://localhost:8080
```
or
```
curl -H "apikey: 0BgkG8uCxGNdraPZHxDz9WcJ39zKtTng" http://localhost:8080
```
4. application reponse will show headers look like
```
{"cmuoauth2user":"supawit.w","cmuoauth2scope":"cmuitaccount.basicinfo","cmuoauth2clientid":"zSnnGPRmAgcskUbEBDqATHYGWYPqftJNXPEvVnMc","cmuoauth2developer":"supawit.w","cmuoauth2granttype":"authorization_code","host":"nginx-oauth-poc-app:8080","connection":"close","user-agent":"curl/7.58.0","accept":"*/*","apikey":"0BgkG8uCxGNdraPZHxDz9WcJ39zKtTng"}
```

## have a nice day :)