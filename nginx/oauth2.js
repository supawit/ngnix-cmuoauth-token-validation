function introspectAccessToken(r) {
    r.subrequest("/_oauth2_send_request", // use this location to send request to token endpoint
        function(reply) {
            if (reply.status == 200) {
                var response = JSON.parse(reply.responseBody);
                if (response.active == true) { // check repose body 
                    
                    /*
                    for(var p in response){
                        if (!response.hasOwnProperty(p)) continue;
                        r.headersOut['OAuth2-Token-' + p] = response[p];
                    }
                    */
                   // send out headers from reponse body
                    r.headersOut['OAuth2-Token-userid']=response.user.user_id;
                    r.headersOut['OAuth2-Token-scope']=response.scope;
                    r.headersOut['OAuth2-Token-clientid']=response.app.client_id;
                    r.headersOut['OAuth2-Token-userdeveloper']=response.app.user_developer;
                    r.headersOut['OAuth2-Token-granttype']=response.app.grant_type_value;
                    r.status = 204;
                    r.sendHeader();
                    r.finish();
                    //r.return(204); // Token is valid, return success code
                } else {
                    r.return(403); // Token is invalid, return forbidden code
                }
            } else {
                r.return(401); // Unexpected response, return 'auth required'
            }
        }
    );
}

function getTokenfromAuthHeader(r){
    var token;
    if(r.headersIn.authorization){
        var res = r.headersIn.authorization.split(" ");
        token = res[1];
    }else if(r.headersIn.apikey){
        token = r.headersIn.apikey;
    }else{
        token = null;
    }
    return token
}